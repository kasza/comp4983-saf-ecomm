import imageminWebp from 'imagemin-webp'
const pkg = require('./package')

module.exports = {
  mode: 'spa',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      // { rel="stylesheet", href="https://fonts.googleapis.com/css?family=Nunito:200,300,400"},
      {
        rel: 'stylesheet',
        href: 'https://use.fontawesome.com/releases/v5.7.1/css/all.css',
        integrity:
          'sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr',
        crossorigin: 'anonymous'
      }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    '~/css/style.css'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    // Doc:https://github.com/nuxt-community/modules/tree/master/packages/bulma
    '@nuxtjs/bulma',
    // Doc:https://github.com/Qonfucius/nuxt-fontawesome
    ['qonfucius-nuxt-fontawesome', { componentName: 'fa-icon',
      packs: [
        {
          package: '@fortawesome/free-solid-svg-icons',
          icons: ['faShoppingCart']
        }
      ],
      includeCss: true }
    ],
    ['nuxt-imagemin', {
      plugins: [
        imageminWebp({ quality: 50 })
      ]
    }],
    'nuxt-webfontloader'
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  **
  */
  webfontloader: {
    google: {
      families: ['Nunito:100,200,300,400'] // Loads Nunito font
    }
  },

  /*
  ** Build configuration
  */
  build: {
    postcss: {
      preset: {
        features: {
          customProperties: false
        }
      }
    },
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
        // config.module.rules.push({
        //   test: /\.(webp)$/i,
        //   loaders: [
        //     'file-loader?hash=sha512&digest=hex&name=[hash].[ext]'
        //   ]
        // })
      }
    }
  }
}
