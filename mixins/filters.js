export default {
  filters: {
    dollar (value) {
      if (!value) {
        return ''
      }

      return `$${parseFloat(Number(value) / 100).toFixed(2)}`
    }
  }
}
