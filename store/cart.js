export const state = () => ({
  cart: {},
  checkoutUrl: null
})

export const getters = {
  cart (state) {
    return state.cart
  },
  checkoutUrl (state) {
    return state.checkoutUrl
  }
}

export const actions = {
  addItem ({ commit }, params) {
    commit('ADD_ITEM', params)
  },
  addQuantity ({ commit }, params) {
    commit('ADD_QUANTITY', params)
  },
  removeItem ({ commit }, index) {
    commit('REMOVE_ITEM', index)
  },
  updateQuantity ({ commit }, params) {
    commit('ADD_ITEM', params)
  },
  async checkout ({ commit, state }) {
    const cartArray = Object.keys(state.cart).map(key => state.cart[key])
    await this.$axios.post('https://square-api.sevenacresfarm.ca/orders/', { items: cartArray })
      .then(({ data }) => {
        commit('SET_CHECKOUT_URL', data.url)
      })
      .catch(error => {
        console.log(error)
      })
  }
}

export const mutations = {
  ADD_ITEM (state, params) {
    const variants = params.product.variants.reduce(function (acc, cur) {
      acc[cur.id] = cur
      return acc
    }, {})
    const variant = variants[params.variantId]
    const quantity = Number(params.quantity) || 1
    let item = {
      name: params.product.name,
      product_id: params.product.id,
      variant_id: variant.id,
      price: variant.price,
      size: variant.size,
      quantity: quantity
    }

    if (state.cart[variant.id]) {
      state.cart[variant.id].quantity += quantity
    } else {
      state.cart[variant.id] = (item)
    }
  },
  ADD_QUANTITY (state, params) {
    state.cart[params.variantId].quantity += params.quantity
  },
  REMOVE_ITEM (state, index) {
    state.cart.splice(index, 1)
  },
  UPDATE_QUANTITY (state, params) {
    let item = state.cart[params.index]
    item.quantity = params.quantity
  },
  SET_CHECKOUT_URL (state, url) {
    state.checkoutUrl = url
  }
}
