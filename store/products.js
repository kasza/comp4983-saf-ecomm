export const state = () => ({
  products: [
    // {
    //   id: 'YYRWYJKTG3UMDIIWDEIO2OCD',
    //   variants: [
    //     {
    //       id: 'B62TF4MRWJFAH7LII26INMFJ',
    //       price: 900,
    //       size: '500 mL'
    //     },
    //     {
    //       id: 'JMAYQGOG54WASIM3PXG2QP5Y',
    //       price: 1300,
    //       size: '750 mL'
    //     }
    //   ],
    //   name: 'Curtido',
    //   description: 'Ingredients: Green Cabbage, Carrot, Onion, Garlic, Oregano, Chili Flakes, Cumin, Sea Salt',
    //   category_id: '4AF5VEIVOY2DZHHV276NF7RK',
    //   category: 'Krauts',
    //   image_url: 'https://square-production.s3.amazonaws.com/files/9ae46f943d9a91c27705dfc6e2182e6a87f35e7c/original.jpeg'
    // }
  ]
})

export const getters = {
  products (state) {
    return state.products
  },
  getProductsByCategory: (state) => (category) => {
    return state.products.filter(product => product.category === category)
  }
}

export const actions = {
  async getProducts ({ commit }) {
    await this.$axios.get('https://square-api.sevenacresfarm.ca/products/')
      .then(({ data }) => {
        commit('SET_PRODUCTS', data)
      })
      .catch(error => {
        console.log(error)
      })
  }
}

export const mutations = {
  SET_PRODUCTS (state, param) {
    state.products = param
  }
}
