# Seven Acres Ferments Ecommerce App - Nuxt Frontend

Master branch automatically deployed to Netlify using webhook.

Hosted at <https://shop.sevenacresfarm.ca>.

Using Nuxt/Netlify for quick development and reviews.



## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

```


